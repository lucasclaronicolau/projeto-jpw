const express = require('express')
const router = express.Router();
const Pessoa = require('../models/pessoa')


router.get('/' , (req, res) =>{
  Pessoa.find(function (err, doc) {
    if (err) return res.status(500).json({ error: "Erro ao consultar pessoas"})
    res.json(doc)
  })
})

router.get('/:id', (req,res) =>{
  Pessoa.findOne({ _id: req.params.id}, function (err, doc) {
    if (err) return res.status(500).json({ error: "Erro ao consultar pessoa"})
    if (doc == null) return res.status(404).json({ error: "Pessoa não encontrada"})
    res.json(doc)
  })
})

router.post('/', (req, res) =>{
  var usuario = new Pessoa(req.body)
  if(usuario.nome == ''){
    return res.status(400).json({ error: "Nome da Pessoa é obrigatório"})
  }
  if(usuario.trein == null){
    return res.status(400).json({ error: "Obrigatório informa se é treinador ou não"})
  }
  usuario.save(function(err, doc){
    if(err) return res.status(500).json({ error: "Erro ao adicionar pessoa"})
    res.json(doc)
  })
})

router.put('/:id', (req,res) =>{
  var usuario = new Pessoa(req.body)
  if(usuario.nome == ''){
    return res.status(400).json({ error: "Nome da Pessoa é obrigatório"})
  }
  Pessoa.findOneAndUpdate({ _id: req.params.id}, req.body, function(err, doc){
    if(err) return res.status(500).json({ error: "Erro ao atualizar pessoa"})
    if (doc == null) return res.status(404).json({ error: "Pessoa não encontrada"})
    return res.json(doc)
  })
})

router.delete('/:id', (req,res) =>{
  var usuario = new Pessoa(req.body)
  Pessoa.findOneAndDelete({ _id: req.params.id}, function(err, doc){
    if(err) return res.status(500).json({ error: "Erro ao excluir pessoa"})
    if (doc == null) return res.status(404).json({ error: "Pessoa não encontrada"})
    return res.json(doc)
  })
})

module.exports = router;
