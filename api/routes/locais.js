const express = require('express')
const router = express.Router();
const Local = require('../models/local')


router.get('/' , (req, res) =>{
  Local.find(function (err, doc) {
    if (err) return res.status(500).json({ error: "Erro ao consultar locais"})
    res.json(doc)
  })
})

router.get('/:id', (req,res) =>{
  Local.findOne({ _id: req.params.id}, function (err, doc) {
    if (err) return res.status(500).json({ error: "Erro ao consultar local"})
    if (doc == null) return res.status(404).json({ error: "Local não encontrado"})
    res.json(doc)
  })
})

router.post('/', (req, res) =>{
  var local = new Local(req.body)
  if(local.nome == ''){
    return res.status(400).json({ error: "Nome da Nome é obrigatório"})
  }
  local.save(function(err, doc){
    if(err) return res.status(500).json({ error: "Erro ao atualizar local"})
    res.json(doc)
  })
})

router.put('/:id', (req,res) =>{
  var local = new Local(req.body)
  if(local.nome == ''){
    return res.status(400).json({ error: "Nome do local é obrigatório"})
  }
  Local.findOneAndUpdate({ _id: req.params.id}, req.body, function(err, doc){
    if(err) return res.status(500).json({ error: "Erro ao atualizar local"})
    if (doc == null) return res.status(404).json({ error: "Local não encontrado"})
    return res.json(doc)
  })
})

router.delete('/:id', (req,res) =>{
  var local = new Local(req.body)
  Local.findOneAndDelete({ _id: req.params.id}, function(err, doc){
    if(err) return res.status(500).json({ error: "Erro ao excluir local"})
    if (doc == null) return res.status(404).json({ error: "Local não encontrado"})
    return res.json(doc)
  })
})

module.exports = router;
