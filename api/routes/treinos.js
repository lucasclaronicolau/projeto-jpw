const express = require('express')
const router = express.Router();
const Treino = require('../models/treino')
const Pessoa = require('../models/pessoa')


router.get('/' , (req, res) =>{
  Treino.find().populate('treinador').exec(function (err, doc) {
    if (err) return res.status(500).json({ error: "Erro ao consultar treinos"})
    res.json(doc)
  })
})

router.get('/:id', (req,res) =>{
  Treino.findOne({ _id: req.params.id}).exec(function (err, doc) {
    if (err) return res.status(500).json({ error: "Erro ao consultar treino"})
    if (doc == null) return res.status(404).json({ error: "Treino não encontrado"})
    res.json(doc)
  })
})

router.post('/', async (req, res) =>{
  var treino = new Treino(req.body)
  var pessoa = await Pessoa.findOne({ _id: treino.treinador})
  if(pessoa != null){
    if(pessoa.trein == false){
      return res.status(400).json({ error: "Pessoa não é um treinador"})
    }
  }
  if(treino.nome == ''){
    return res.status(400).json({ error: "Nome do treino é obrigatório"})
  }
  if(treino.treinador == null){
    return res.status(400).json({ error: "Treinador do treino é obrigatório"})
  }
  if(treino.alunos == null){
    return res.status(400).json({ error: "Alunos do treino são obrigatório"})
  }
  treino.save(function(err, doc){
    if(err) return res.status(500).json({ error: "Erro ao adicionar treino"})
    res.json(doc)
  })
})

router.put('/:id', async (req,res) =>{
  var treino = new Treino(req.body)
  var pessoa = await Pessoa.findOne({ _id: treino.treinador})
  if(pessoa != null){
    if(pessoa.trein == false){
      return res.status(400).json({ error: "Pessoa não é um treinador"})
    }
  }
  if(treino.nome == ''){
    return res.status(400).json({ error: "Nome do treino é obrigatório"})
  }
  if(treino.treinador == null){
    return res.status(400).json({ error: "Treinador do treino é obrigatório"})
  }
  if(treino.alunos == null){
    return res.status(400).json({ error: "Alunos do treino são obrigatório"})
  }
  Treino.findOneAndUpdate({ _id: req.params.id}, req.body, function(err, doc){
    if(err) return res.status(500).json({ error: "Erro ao atualizar treino"})
    if (doc == null) return res.status(404).json({ error: "Treino não encontrada"})
    return res.json(doc)
  })
})

router.delete('/:id', (req,res) =>{
  var treino = new Treino(req.body)
  Treino.findOneAndDelete({ _id: req.params.id}, function(err, doc){
    if(err) return res.status(500).json({ error: "Erro ao excluir equipamento"})
    if (doc == null) return res.status(404).json({ error: "Treino não encontrado"})
    return res.json(doc)
  })
})

module.exports = router;
