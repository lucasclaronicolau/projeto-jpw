const express = require('express')
const router = express.Router();
const Equipamento = require('../models/equipamento')


router.get('/' , (req, res) =>{
  Equipamento.find(function (err, doc) {
    if (err) return res.status(500).json({ error: "Erro ao consultar equipamentos"})
    res.json(doc)
  })
})

router.get('/:id', (req,res) =>{
  Equipamento.findOne({ _id: req.params.id}, function (err, doc) {
    if (err) return res.status(500).json({ error: "Erro ao consultar equipamento"})
    if (doc == null) return res.status(404).json({ error: "Equipamento não encontrado"})
    res.json(doc)
  })
})

router.post('/', (req, res) =>{
  var equipamento = new Equipamento(req.body)
  if(equipamento.nome == ''){
    return res.status(400).json({ error: "Nome do Equipamento é obrigatório"})
  }
  equipamento.save(function(err, doc){
    if(err) return res.status(500).json({ error: "Erro ao adicionar equipamento"})
    res.json(doc)
  })
})

router.put('/:id', (req,res) =>{
  var equipamento = new Equipamento(req.body)
  if(equipamento.nome == ''){
    return res.status(400).json({ error: "Nome do Equipamento é obrigatório"})
  }
  Equipamento.findOneAndUpdate({ _id: req.params.id}, req.body, function(err, doc){
    if(err) return res.status(500).json({ error: "Erro ao atualizar equipamento"})
    if (doc == null) return res.status(404).json({ error: "Equipamento não encontrado"})
    return res.json(doc)
  })
})

router.delete('/:id', (req,res) =>{
  var equipamento = new Equipamento(req.body)
  Equipamento.findOneAndDelete({ _id: req.params.id}, function(err, doc){
    if(err) return res.status(500).json({ error: "Erro ao excluir equipamento"})
    if (doc == null) return res.status(404).json({ error: "Equipamento não encontrado"})
    return res.json(doc)
  })
})

module.exports = router;
