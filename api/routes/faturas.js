const express = require('express')
const router = express.Router();
const Fatura = require('../models/fatura')


router.get('/' , (req, res) =>{
  Fatura.find().populate('pessoa').exec(function (err, doc) {
    if (err) return res.status(400).json({ error: "Erro ao consultar faturas"})
    res.json(doc)
  })
})

router.get('/:id', (req,res) =>{
  Fatura.findOne({ _id: req.params.id}).populate('pessoa').exec(function (err, doc) {
    if (err) return res.status(500).json({ error: "Erro ao consultar fatura"})
    if (doc == null) return res.status(404).json({ error: "Fatura não encontrada"})
    res.json(doc)
  })
})

router.post('/', (req, res) =>{
  var fatura = new Fatura(req.body)
  if(fatura.issuedate == null){
    return res.status(400).json({ error: "Data de emissão é obrigatória"})
  }
  if(fatura.duedate == null){
    return res.status(400).json({ error: "Data de vencimento é obrigatória"})
  }
  if(fatura.pessoa == null){
    return res.status(400).json({ error: "Pessoa é obrigatório"})
  }
  fatura.save(function(err, doc){
    if(err) return res.status(500).json({ error: "Erro ao adicionar fatura"})
    res.json(doc)
  })
})

router.put('/:id', (req,res) =>{
  var fatura = new Fatura(req.body)
  if(fatura.issuedate == null){
    return res.status(400).json({ error: "Data de emissão é obrigatória"})
  }
  if(fatura.duedate == null){
    return res.status(400).json({ error: "Data de vencimento é obrigatória"})
  }
  if(fatura.pessoa == null){
    return res.status(400).json({ error: "Pessoa é obrigatório"})
  }
  Fatura.findOneAndUpdate({ _id: req.params.id}, req.body).populate('pessoa').exec(function (err, doc) {
    if(err) return res.status(500).json({ error: "Erro ao atualizar fatura"})
    if (doc == null) return res.status(404).json({ error: "Fatura não encontrada"})
    return res.json(doc)
  })
})

router.delete('/:id', (req,res) =>{
  var fatura = new Fatura(req.body)
  Fatura.findOneAndDelete({ _id: req.params.id}, function(err, doc){
    if(err) return res.status(500).json({ error: "Erro ao excluir fatura"})
    if (doc == null) return res.status(404).json({ error: "Fatura não encontrada"})
    return res.json(doc)
  })
})

module.exports = router;
