const mongoose = require('../data');

var treinoSchema = new mongoose.Schema({
  nome: {type: String,required: true},
  treinador : {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Pessoa',
    required: true
  },
  alunos : [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Pessoa',
    required: true
  }],
  equip: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Equipamento'
  }]
})

var treino = mongoose.model('Treino', treinoSchema);

module.exports = treino;
