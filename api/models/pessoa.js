const mongoose = require('../data');

var pessoaSchema = new mongoose.Schema({
  nome: {type: String, required: true},
  end: String,
  nasc: Date,
  trein: {type: Boolean, required: true},
})
var pessoa = mongoose.model('Pessoa', pessoaSchema);

module.exports = pessoa;
