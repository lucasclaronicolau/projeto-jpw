const mongoose = require('../data');

var equipamentoSchema = new mongoose.Schema({
  nome: {type: String,
    required: true},
  qnt: Number,
})
var equipamento = mongoose.model('Equipamento', equipamentoSchema);

module.exports = equipamento;
