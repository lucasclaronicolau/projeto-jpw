const mongoose = require('../data');

var userSchema = new mongoose.Schema({
  authorization: {type: String,required: true},
})
var user = mongoose.model('User', userSchema);

module.exports = user;
