const mongoose = require('../data');

var faturaSchema = new mongoose.Schema({
  issuedate: {type: Date, required: true},
  duedate: {type: Date, required: true},
  pessoa : {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Pessoa',
    required: true
  }
})

var fatura = mongoose.model('Fatura', faturaSchema);

module.exports = fatura;
