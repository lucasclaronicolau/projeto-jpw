# Projeto API Academia
Este projeto tem como objetivo a contrução de uma API utilizante REST. Foi criado um mini gerenciado de academia, cuidando do cadastro de pessoas (Treinado ou Usuário), Equipamentos, Faturas, Locais e Treinos;

## Get Started

Para instalar todas as depêndencias bastas utilizar o seguinte comando:
```javascript
npm install
```
Logo após, basta executar o comando abaixo para executar a aplicação:
```javascript
npm start
```

**_Para ter acesso aos endpoint deve ser enviado o um dos tokens armazenados no banco no authorization do header(Caso seja necessário mais de um token deve ser cadastro manualmente no banco)_**
<p></p>

## Lista de Models e Endpoint

### Equipamentos
```javascript
{
    "_id" : "String",
    "nome" : "String" [required],
    "qnt" : 0
}
```
GET
```javascript
url: /equipamentos
```
GET
```javascript
url: /equipamentos/:id
```
POST
```javascript
url: /equipamentos

{
    "nome" : "String" [required],
    "qnt" : 0
}
```
PUT
```javascript
url: /equipamentos/:id

{
    "nome" : "String" [required],
    "qnt" : 0
}
```
DELETE
```javascript
url: /equipamentos/:id
```

### Faturas
```javascript
{
    "_id" : "String",
    "issuedate" : Date [required],
    "duedate" : Date [required],
    "pessoa" : Pessoa [required]
}
```
GET
```javascript
url: /faturas
```
GET
```javascript
url: /faturas/:id
```
POST
```javascript
url: /faturas

{
    "issuedate" : Date [required],
    "duedate" : Date [required],
    "pessoa" : Pessoa_id [required]
}
```
PUT
```javascript
url: /faturas/:id

{
    "issuedate" : Date [required],
    "duedate" : Date [required],
    "pessoa" : Pessoa_id [required]
}
```
DELETE
```javascript
url: /faturas/:id
```

### Locais
```javascript
{
    "_id" : "String",
    "nome" : "String" [required],
    "end" : "String"
}
```
GET
```javascript
url: /locais
```
GET
```javascript
url: /locais/:id
```
POST
```javascript
url: /locais

{
    "nome" : "String" [required],
    "end" : "String"
}
```
PUT
```javascript
url: /locais/:id

{
    "nome" : "String" [required],
    "end" : "String"
}
```
DELETE
```javascript
url: /locais/:id
```

### Pessoas
```javascript
{
    "_id" : "String",
    "nome" : "String" [required],
    "end" : "String",
    "nasc" : Date,
    "trein" : Boolean
}
```
GET
```javascript
url: /pessoas
```
GET
```javascript
url: /pessoas/:id
```
POST
```javascript
url: /pessoas

{
    "nome" : "String" [required],
    "end" : "String",
    "nasc" : Date,
    "trein" : Boolean
}
```
PUT
```javascript
url: /pessoas/:id

{
    "nome" : "String" [required],
    "end" : "String",
    "nasc" : Date,
    "trein" : Boolean
}
```
DELETE
```javascript
url: /pessoas/:id
```

### Treinos
```javascript
{
    "_id" : "String",
    "nome" : "String" [required],
    "treinador" : Pessoa,
    "alunos" : [Pessoa],
    "equip" : [Equipamento]
}
```
GET
```javascript
url: /treinos
```
GET
```javascript
url: /treinos/:id
```
POST
```javascript
url: /treinos

{
    "nome" : "String" [required],
    "treinador" : Pessoa_id,
    "alunos" : [Pessoa_id],
    "equip" : [Equipamento-id]
}
```
PUT
```javascript
url: /treinos/:id

{
    "nome" : "String" [required],
    "treinador" : Pessoa_id,
    "alunos" : [Pessoa_id],
    "equip" : [Equipamento_id]
}
```
DELETE
```javascript
url: /treinos/:id
```
