﻿const express = require('express');
const pessoas = require('./api/routes/pessoas.js');
const equipamentos = require('./api/routes/equipamentos.js');
const treinos = require('./api/routes/treinos.js');
const locais = require('./api/routes/locais.js');
const faturas = require('./api/routes/faturas.js');
const User = require('./api/models/user.js')
const app = express();
var cors = require('cors');

app.use(cors());
app.use(express.json())
app.use(function (req, res, next) {
	var ok = false;
	User.find(function (err, doc) {
		doc.forEach(function(value){
			if(value.authorization == req.headers.authorization){
				ok =  true;
			}
		});
		if(ok){
			next();
		}
		else{
			return res.status(401).json({ error: "Não Autorizado"});
		}
	})
})
app.use('/pessoas', pessoas);
app.use('/equipamentos', equipamentos);
app.use('/treinos', treinos);
app.use('/locais', locais);
app.use('/faturas', faturas);

app.listen(5000, () =>{
	console.log('Rodando');
})
